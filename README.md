![N|Solid](logo_ioasys.png)

# Desafio React Native - ioasys


### Bibliotecas usadas

* apisauce
Uma lib que contém Axios + standardized errors + request/response transforms facilitando a forma de tratar requisições.
* prop-types
Uma lib para documentar e checar os propriedades passados para componentes.
* ramda
Uma lib com funções de paragdima funcional que auxilia em diversas operações.
* react-native-gesture-handler
Uma lib usada juntamente com o react-nativation para gerenciamento de touchs.
* react-native-linear-gradient
Um componente que exerce a mesma função de um Linear Gradient de forma nativa.
* react-navigation
Uma lib para navegação entre telas.
* react-redux
Um binding do Redux para React.
* redux-saga
Uma lib usada para facilitar o tratamento de chamadas assíncronas contornando erros.
* reduxsauce
Uma lib que fornece algumas ferramentas para trabalhar com Redux, usada na construção de reducers com Duck Pattern.
* seamless-immutable
Uma lib alternativa a ImmutableJS usada nos dados armazenados no fluxo do redux.

### Como executar

Baixar arquivo [here](ioasysEmpresas.apk) no seu celular android, clicar no arquivo baixado para instalá-lo.
O smartphone deve estar com a fontes desconhecidas habilitado.

### Features pensadas não implementadas

Devido ao tempo algumas features pensadas não foram implementadas, essas são: 

* Olho no input de senha para exibir e ocultar o conteúdo
* Efeito de placeholder usado com lib rn-placeholder no carregamento dos cards
* Tela com detalhes de enterprise