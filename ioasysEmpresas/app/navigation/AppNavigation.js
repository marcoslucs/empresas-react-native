import { createStackNavigator, createAppContainer } from "react-navigation"
import SplashScreen from "../containers/SplashScreen";
import LoginScreen from "../containers/LoginScreen";
import EnterprisesScreen from "../containers/EnterprisesScreen";

const AppNavigator = createStackNavigator(
  {
    SplashScreen: { screen: SplashScreen },
    LoginScreen: { screen: LoginScreen },
    EnterprisesScreen: { screen: EnterprisesScreen }
  },
  {
    initialRouteName: "SplashScreen"
  }
);

export default createAppContainer(AppNavigator);
