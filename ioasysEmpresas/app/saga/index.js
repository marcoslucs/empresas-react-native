import { takeLatest, all } from 'redux-saga/effects'
import API from '../services/api'

/* ------------- Types ------------- */
import { AuthTypes } from '../redux/AuthRedux'
import { EnterprisesTypes } from '../redux/EnterprisesRedux'

/* ------------- Sagas ------------- */
import { authSaga } from './AuthSaga'
import { enterprisesSaga, enterprisesByTypeSaga } from './EnterprisesSaga'

const api = API.create()

export default function * root () {
  /* ------------- Connecting Redux Actions to imported Sagas ------------- */
  yield all([
    takeLatest(AuthTypes.AUTHENTICATE_USER_REQUEST, authSaga, api),
    takeLatest(EnterprisesTypes.ENTERPRISES_REQUEST, enterprisesSaga, api),
    takeLatest(EnterprisesTypes.ENTERPRISES_BY_TYPE_REQUEST, enterprisesByTypeSaga, api),
  ])
}