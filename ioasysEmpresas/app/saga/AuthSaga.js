import { call, put } from 'redux-saga/effects'
import AuthActions from '../redux/AuthRedux'

export function* authSaga (api, action) {
  const response = yield call(api.authSignIn, action.authData)
  if (response.ok) {
    yield api.configDynamicHeaders({
      'client': response.headers.client,
      'access-token': response.headers['access-token'],
      'uid': response.headers.uid
    })
    yield put(AuthActions.authenticateUserSuccess(response.data))
  } else {
    yield put(AuthActions.authenticateUserError(response.data))
  }
}
