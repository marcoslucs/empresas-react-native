import { call, put } from 'redux-saga/effects'
import EnterprisesActions from '../redux/EnterprisesRedux'
import { contains } from 'ramda'

export function* enterprisesSaga (api, action) {
  const response = yield call(api.getEnterprises)
  if (response.ok) {
    yield put(EnterprisesActions.enterprisesSuccess(prepareEnterprisesData(response.data.enterprises)))
  } else {
    yield put(EnterprisesActions.enterprisesError(response))
  }
}

export function* enterprisesByTypeSaga (api, action) {
  const response = yield call(api.getEnterprisesByType, action.enterprisesFilter)
  if (response.ok) {
    yield put(EnterprisesActions.enterprisesByTypeSuccess(extractEnterprises(response.data.enterprises)))
  } else {
    yield put(EnterprisesActions.enterprisesByTypeError(response))
  }
}

const prepareEnterprisesData = (data) => {
  let preparedData = {
    enterprises: [],
    enterprise_types: [{ id: -1, enterprise_type_name: 'All'}]
  }

  if ( data.length > 0 ) {
    preparedData.enterprises = extractEnterprises(data)
    preparedData.enterprise_types = preparedData.enterprise_types.concat(extractEnterprisesTypes(data))
  }

  return preparedData
}

const extractEnterprises = (data) => {
  return data.map( value => {
    return {
      id: value.id,
      enterprise_name: value.enterprise_name, 
      city: value.city,
      country: value.country,
      photo: value.photo
    }
  })
}

const extractEnterprisesTypes = (data) => {
  let agrupedEnterpriseTypes = data.reduce( 
    (enterprise_types_acumulator, enterprise) => {
      if(!contains(enterprise.enterprise_type, enterprise_types_acumulator))
        enterprise_types_acumulator.push(enterprise.enterprise_type)
      return enterprise_types_acumulator
    }, [] )

  return agrupedEnterpriseTypes.sort((a, b) =>
    a.enterprise_type_name > b.enterprise_type_name
      ? 1 
      : b.enterprise_type_name > a.enterprise_type_name
        ? -1 
        : 0 
  )
}
