export function validateEmail (email) {
  let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
  return ((reg.test(email) === true) && email.length > 5 && email.length <= 50)
}

export function validatePassword (password) {
  return (password.length > 0)
}

const validators = {
    validatePassword: validatePassword,
    validateEmail: validateEmail
};
export default validators;
