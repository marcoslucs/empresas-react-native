import { AsyncStorage } from "react-native";

export const persistItem = async (key, value, callback) => {
  try {
    return await AsyncStorage.setItem(key, value, callback);
  } catch (error) {
    handlePersistenceError(error);
  }
};

export const retrieveItem = async (key, callback) => {
  try {
    return await AsyncStorage.getItem(key, callback);
  } catch (error) {
    handlePersistenceError(error);
  }
};

export const deleteItem = async (key, callback) => {
  try {
    return await AsyncStorage.removeItem(key, callback);
  } catch (error) {
    handlePersistenceError(error);
  }
};

const handlePersistenceError = error => console.log("PERSISTENCE ERROR: ", error.message);
