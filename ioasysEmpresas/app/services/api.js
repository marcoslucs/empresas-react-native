import apisauce from 'apisauce'

const url = ' http://empresas.ioasys.com.br/'

const create = (baseURL = url) => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache',
      'Content-Type': 'application/json'
    },
    timeout: 10000
  })

  return {
    api,
    configDynamicHeaders: headerData => { api.setHeaders({
        ...api.headers,
        'client': headerData.client,
        'access-token': headerData['access-token'].trim(),
        'uid': headerData.uid
      })
    },
    authSignIn: (authData) => api.post('api/v1/users/auth/sign_in', authData),
    getEnterprises: () => api.get('api/v1/enterprises'),
    getEnterprisesByType: (enterprisesFilter) => api.get(`api/v1/enterprises?enterprise_types=${enterprisesFilter.enterprise_types}`)
  }
}

export default { create, url }