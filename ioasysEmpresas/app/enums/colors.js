export const colors = {
  blue: "#86A8E7",
  lightenBlue: "#BBDCFF",
  white: "#FFF",
  gray: '#A2A2A2',
  lightenGray: "#BDBDBD",
  darkenGray: "#E9E9E9",
  red: "#E24A5E"
}