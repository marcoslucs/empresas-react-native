
import { connect } from "react-redux"
import React, {Component} from 'react'
import { View, FlatList, Image, Text } from 'react-native'
import EnterprisesActions from '../redux/EnterprisesRedux'
import {equals} from 'ramda'
import Card from '../components/Card'
import BadgeButton from '../components/BadgeButton'
import LogoEmpresas from '../components/LogoEmpresas'
import mainStyles from './styles/MainApplicationStyles'
import styles from './styles/EnterprisesStyles'
export class EnterprisesScreen extends Component {

  static navigationOptions = {
    headerTitle: (
      <View style={{flex:1, flexDirection: 'row', justifyContent: 'center'}}>
        <LogoEmpresas width={50} height={35}/> 
      </View>
    )
  }

  constructor (props) {
    super(props)
    this.state = {
      refreshing: false
    }
  }

  componentWillMount () { this._fetchEnterprises() }

  componentDidUpdate (prevProps) {
    if (!equals(prevProps.enterprisesFilter, this.props.enterprisesFilter)) {
      this._fetchEnterprises()
    }
  }

  _onRefresh = () => {
    this._fetchEnterprises()
    setTimeout(() => this.setState({ refreshing: false}), 1000)
  }

  _fetchEnterprises () {
    this.props.enterprisesFilter.enterprise_types === -1 
      ? this.props.getEnterprises()
      : this.props.getEnterprisesByType(this.props.enterprisesFilter)
  }

  _renderCardItem ({item}) {
    return (
      <Card elevation={3} paddingVertical={10}
        paddingHorizontal={20} marginVertical={8} marginHorizontal={16}>
        <View style={mainStyles.flexRowAlignCenterJustifyBetween}>
          <View style={mainStyles.flex}>
            <Text style={mainStyles.font16Bold}>{item.enterprise_name}</Text>
            <Text style={mainStyles.font12}>{item.city} - {item.country}</Text>
          </View>
          <Image source={ item.photo 
              ? { uri: `http://empresas.ioasys.com.br${item.photo}` }
              : require('../images/png/logo.png')
          } style={[styles.imageDefault, { resizeMode: !item.photo ? "contain" : null }]}/>
        </View>
      </Card>
    ) 
  }

  render() {
    return (
      <View style={mainStyles.flex}>
        <FlatList
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          style={styles.filterContainer}
          data={this.props.enterprisesTypes}
          extraData={this.props}
          keyExtractor={item => item.id+this.props.enterprisesFilter.enterprise_types}
          renderItem={({item}) => <BadgeButton
              onPress={() => this.props.setEnterprisesFilter({ enterprise_types: item.id })}
              text={item.enterprise_type_name}
              isSelected={ item.id === this.props.enterprisesFilter.enterprise_types}/>
          }/>
        <FlatList
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh}
          style={styles.cardsContainer}
          data={this.props.enterprises}
          extraData={this.state}
          keyExtractor={item => item.id}
          renderItem={this._renderCardItem}/>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  enterprises: state.enterprises.enterprisesData.enterprises,
  enterprisesTypes: state.enterprises.enterprisesData.enterprise_types,
  enterprisesFilter: state.enterprises.enterprisesFilter,
  fetching: state.enterprises.fetchings
})

const mapDispatchToProps = dispatch => {
  return {
    getEnterprises: () => dispatch(EnterprisesActions.enterprisesRequest()),
    setEnterprisesFilter: (newFilter) => dispatch(EnterprisesActions.setEnterprisesFilter(newFilter)),
    getEnterprisesByType: (filter) => dispatch(EnterprisesActions.enterprisesByTypeRequest(filter))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EnterprisesScreen)