
import { connect } from "react-redux"
import React, {Component} from 'react'
import { Text } from 'react-native'
import { ScrollView } from "react-native-gesture-handler"
import { NavigationActions } from 'react-navigation'
import AuthActions from '../redux/AuthRedux'
import LogoEmpresas from '../components/LogoEmpresas'
import GradientButton from '../components/GradientButton'
import TextField from '../components/TextField'
import mainStyles from './styles/MainApplicationStyles'
import styles from './styles/LoginScreenStyles'
import { validateEmail, validatePassword } from "../utils/Validators";

export class LoginScreen extends Component {

  static navigationOptions = { header: null }

  constructor (props) {
    super(props)
    this.state = {
      email: '',
      password: ''
    }
  }

  componentDidUpdate () {
    if (this.props.isLogged) {
      this.props.navigation.reset([NavigationActions.navigate({ routeName: 'EnterprisesScreen' })], 0)
    }
  }

  _validateInputs = () => {
    return !( validateEmail(this.state.email) && 
      validatePassword(this.state.password) )
  }

  render() {
    return (
      <ScrollView keyboardShouldPersistTaps={'handled'} contentContainerStyle={mainStyles.flexAlignJustifyCenter}>
        <LogoEmpresas marginBottom={20}/>
        { 
          this.props.error 
            ? <Text style={mainStyles.errorText}>{this.props.errorMessage}</Text>
            : null
        }
        <Text style={styles.labelInputs}>Username:</Text>
        <TextField onChangeText={ text => this.setState({email: text}) }/>
        <Text style={styles.labelInputs}>Password:</Text>
        <TextField secureTextEntry 
          onChangeText={ text => this.setState({password: text}) }/>
        <GradientButton text={this.props.fetching ? "ENTRANDO ..." : "ENTRAR"} disabled={this._validateInputs()}
          onPress={() => this.props.login({ email: this.state.email, password: this.state.password }) }/>
      </ScrollView>
    )
  }
}

const mapStateToProps = state => ({
  isLogged: state.auth.isLogged,
  error: state.auth.error,
  fetching: state.auth.fetching,
  errorMessage: state.auth.errorMessage
})

const mapDispatchToProps = dispatch => {
  return { login: (data) => dispatch(AuthActions.authenticateUserRequest(data)) }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)