
import React, {Component} from 'react';
import { View, Image } from 'react-native';
import { NavigationActions } from 'react-navigation';
import mainStyles from './styles/MainApplicationStyles'

export default class SplashScreen extends Component {

  static navigationOptions = { header: null }

  componentDidMount() {
    setTimeout( () => this.props.navigation.reset([NavigationActions.navigate({ routeName: 'LoginScreen' })], 0), 5000)
  }

  render () {
    return (
      <View style={mainStyles.flexAlignJustifyCenter}>
        <Image source={require('../images/png/logo.png')} />
      </View>
    )
  }
}