import { StyleSheet } from 'react-native'
import { colors } from '../../enums/colors';

export default StyleSheet.create({
  imageDefault: {
    width: 100,
    height: 100,
    borderRadius: 100,
    alignSelf: 'flex-end'
  },
  filterContainer: {
    maxHeight: 60, 
    minHeight: 60, 
    paddingVertical: 15, 
    backgroundColor: colors.darkenGray, 
    alignContent: 'center'
  },
  cardsContainer: {
    paddingTop: 10
  }
})