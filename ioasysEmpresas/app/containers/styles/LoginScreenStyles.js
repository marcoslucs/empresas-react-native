import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  labelInputs: {
    alignSelf: 'flex-start',
    left: '10%'
  }
})