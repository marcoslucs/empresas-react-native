import { StyleSheet } from 'react-native'
import { colors } from '../../enums/colors';

export default StyleSheet.create({
  flex: {
    flex: 1
  },
  flexAlignJustifyCenter: {
    flex:1, 
    justifyContent: 'center', 
    alignItems: 'center'
  },
  flexRowAlignCenterJustifyBetween: {
    flex:1, 
    flexDirection: 'row',
    justifyContent: 'space-between', 
    alignItems: 'center'
  },
  font16Bold: {
    fontSize: 16,
    fontWeight: '700'
  },
  font12: {
    fontSize: 12,
    fontWeight: '400'
  },
  errorText: {
    fontSize: 16, 
    color: colors.red, 
    marginBottom: 20
  }
})