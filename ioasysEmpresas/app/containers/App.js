import { Provider } from 'react-redux'
import React from 'react'
import store from "../redux/store"
import Navigator from "../navigation/AppNavigation"
import { StatusBar } from 'react-native'

export default () => 
  <Provider store={store}>
    <StatusBar backgroundColor="#e7e7e7" barStyle="light-content" />
    <Navigator/>
  </Provider>
