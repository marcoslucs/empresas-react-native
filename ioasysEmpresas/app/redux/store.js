import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";

import reducers from "./";
import sagas from "../saga/";

// create the saga middleware
const sagaMiddleware = createSagaMiddleware();

// mount it on the Store
export default createStore(reducers, applyMiddleware(sagaMiddleware));

// then run the saga
sagaMiddleware.run(sagas);