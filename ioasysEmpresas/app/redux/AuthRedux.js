import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({

  authenticateUserRequest: (authData) => {
    return { type: 'AUTHENTICATE_USER_REQUEST', authData }
  },
  authenticateUserSuccess: (response) => {
    return { type: 'AUTHENTICATE_USER_SUCCESS', authResponse: response }
  },
  authenticateUserError: (errorData) => {
    return { type: 'AUTHENTICATE_USER_ERROR', errorData }
  }

})

export const AuthTypes = Types
export default Creators

/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
  isLogged: false,
  userData: null,
  fetching: false,
  error: false,
  errorMessage: null
})

/* ------------- Reducers ------------- */

export const authenticateUserRequestReducer = (state) => {
  return { 
    ...state, 
    error: false,
    fetching: true, 
    isLogged: false,
    errorMessage: null
  }
}

export const authenticateUserSuccessReducer = (state, { authResponse })  => {
  return { 
    ...state, 
    fetching: false, 
    isLogged: true,
    userData: authResponse.investidor
  }
}

export const authenticateUserErrorReducer = (state, { errorData }) => {
  return { 
    ...state, 
    fetching: false, 
    error: true, 
    isLogged: false,
    userData: null,
    errorMessage: errorData.errors[0]
  }
}

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
  [Types.AUTHENTICATE_USER_REQUEST]: authenticateUserRequestReducer,
  [Types.AUTHENTICATE_USER_SUCCESS]: authenticateUserSuccessReducer,
  [Types.AUTHENTICATE_USER_ERROR]: authenticateUserErrorReducer
});