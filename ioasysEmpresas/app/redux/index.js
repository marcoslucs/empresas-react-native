import { combineReducers } from 'redux'

export default reducers = combineReducers({
  auth: require('./AuthRedux').reducer,
  enterprises: require('./EnterprisesRedux').reducer
})