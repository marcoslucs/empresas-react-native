import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({

  setEnterprisesFilter: (enterprisesFilter) => {
    return { type: 'SET_ENTERPRISES_FILTER', enterprisesFilter }
  },

  /* -- get enterprises by category -- */

  enterprisesByTypeRequest: (enterprisesFilter) => {
    return { type: 'ENTERPRISES_BY_TYPE_REQUEST', enterprisesFilter}
  },

  enterprisesByTypeSuccess: (enterprisesByTypeData) => {
    return { type: 'ENTERPRISES_BY_TYPE_SUCCESS', enterprisesByTypeData}
  },

  enterprisesByTypeError: (errorData) => {
    return { type: 'ENTERPRISES_BY_TYPE_ERROR', errorData}
  },

  /* -- get all enterprises -- */

  enterprisesRequest: () => {
    return { type: 'ENTERPRISES_REQUEST' }
  },

  enterprisesSuccess: (enterprisesData) => {
    return { type: 'ENTERPRISES_SUCCESS', enterprisesData }
  },

  enterprisesError: (errorData) => {
    return { type: 'ENTERPRISES_ERROR', errorData }
  }

})

export const EnterprisesTypes = Types
export default Creators


/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
  enterprisesData: {
    enterprises: [],
    enterprise_types: []
  },
  enterprisesFilter: {
    enterprise_types: -1
  },
  fetching: false,
  error: false
})

/* ------------- Reducers ------------- */
export const requestRequest = (state)  => { 
  return {
    ...state,
    fetching: true,
    error: false
  }
}

export const enterprisesSuccessReducer = (state, {enterprisesData}) => {
  return {
    ...state,
    fetching: false,
    enterprisesData
  }
}

export const enterprisesErrorReducer = (state, {error}) => {
  return {
    ...state,
    error: true,
    fetching: false
  }
}

export const enterprisesByTypeSuccessReducer = (state, {enterprisesByTypeData}) => {
  return {
    ...state,
    fetching: false,
    enterprisesData: {
      enterprises: enterprisesByTypeData,
      enterprise_types: state.enterprisesData.enterprise_types
    }
  }
}


export const enterprisesByTypeErrorReducer = (state, {errorData}) => {
  return {
    ...state,
    error: true,
    fetching: false
  }
}

export const setEnterprisesFilterReducer = ( state, { enterprisesFilter }) => {
  return {
    ...state,
    enterprisesFilter
  }
}

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
  [Types.ENTERPRISES_REQUEST]: requestRequest,
  [Types.ENTERPRISES_SUCCESS]: enterprisesSuccessReducer,
  [Types.ENTERPRISES_ERROR]: enterprisesErrorReducer,
  [Types.ENTERPRISES_BY_TYPE_REQUEST]: requestRequest,
  [Types.ENTERPRISES_BY_TYPE_SUCCESS]: enterprisesByTypeSuccessReducer,
  [Types.ENTERPRISES_BY_TYPE_ERROR]: enterprisesByTypeErrorReducer,
  [Types.SET_ENTERPRISES_FILTER]: setEnterprisesFilterReducer
});