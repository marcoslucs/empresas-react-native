import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, Text } from 'react-native'
import styles from './styles/BadgeButtonStyles'
import { colors } from '../enums/colors'

export default class BadgeButton extends Component {

  // Prop type warnings
  static propTypes = {
    text: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired,
    isSelected: PropTypes.bool.isRequired,
    backgroundColorSelected: PropTypes.string,
    backgroundColorNoSelected: PropTypes.string,
  }

  // Defaults for props
  static defaultProps = {
    text: "",
    onPress: () => null,
    isSelected: false,
    backgroundColorSelected: colors.blue,
    backgroundColorNoSelected: colors.lightenGray,
  }

  render () {
    return (
      <TouchableOpacity 
        onPress={this.props.onPress}
        activeOpacity={0.8}
        style={[styles.badgeButton, {
          backgroundColor: this.props.isSelected 
            ? this.props.backgroundColorSelected 
            : this.props.backgroundColorNoSelected 
        }]}>
          <Text style={styles.buttonText}>
            {this.props.text}
          </Text>
      </TouchableOpacity>
    )
  }
}
