import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TextInput } from 'react-native'
import styles from './styles/TextFieldStyle'

export default class TextField extends Component {

  // Prop type warnings
  static propTypes = {
    onChangeText: PropTypes.func.isRequired,
    secureTextEntry: PropTypes.bool
  }

  // Defaults for props
  static defaultProps = {
    secureTextEntry: false
  }

  render () {
    return (
      <TextInput
        secureTextEntry={this.props.secureTextEntry}
        onChangeText={ text => this.props.onChangeText(text)}
        style={styles.textField}
        blurOnSubmit={true}
        maxLength={this.props.maxLength}
        underlineColorAndroid={'transparent'}
        multiline={false}/>
    )
  }
}
