
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Image } from 'react-native'

export default class LogoEmpresas extends Component {

   // Prop type warnings
   static propTypes = {
    marginBottom: PropTypes.number,
    height: PropTypes.number,
    width: PropTypes.number
  }

  // Defaults for props
  static defaultProps = {
    marginBottom: 0,
    height: 120,
    width: 160
  }

  render () {
    return <Image style={{
        width: this.props.width,
        height: this.props.height,
        marginBottom: this.props.marginBottom
      }} source={require('../images/png/logo.png')} />
  }
} 