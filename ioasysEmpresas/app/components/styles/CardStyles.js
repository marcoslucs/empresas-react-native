import { StyleSheet } from 'react-native'
import { colors } from '../../enums/colors'

export default StyleSheet.create({
  card: {
    flex: 1,
    backgroundColor: colors.white
  },
})