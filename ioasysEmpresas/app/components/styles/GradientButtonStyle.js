import { StyleSheet } from 'react-native'
import { colors } from '../../enums/colors'

export default StyleSheet.create({
  gradientButton: {
    borderRadius: 5,
    padding: 10
  },
  buttonText: {
    textAlign: 'center',
    color: colors.white
  }
})
