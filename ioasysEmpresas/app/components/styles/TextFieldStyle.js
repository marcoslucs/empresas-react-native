import { StyleSheet } from 'react-native'
import { colors } from '../../enums/colors'

export default StyleSheet.create({
  textField: {
    paddingHorizontal: 6,
    marginVertical: 5,
    textAlign: 'left',
    height: 35,
    borderWidth: 0.3,
    borderRadius: 4,
    borderColor: colors.grey,
    width: "80%"
  }
})