import { StyleSheet } from 'react-native'
import { colors } from '../../enums/colors'

export default StyleSheet.create({
  badgeButton: {
    padding: 10,
    borderRadius: 8,
    justifyContent: 'center',
    marginHorizontal: 5,
  },
  buttonText: {
    textAlign: 'center',
    color: colors.white
  }
})