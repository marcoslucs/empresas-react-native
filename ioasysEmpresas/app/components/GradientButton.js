import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, Text } from 'react-native'
import styles from './styles/GradientButtonStyle'
import LinearGradient from 'react-native-linear-gradient'
import { colors } from '../enums/colors'

export default class GradientButton extends Component {

  // Prop type warnings
  static propTypes = {
    text: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired,
    width: PropTypes.string,
    initialGradientColor: PropTypes.string,
    finalGradientColor: PropTypes.string,
    textStyle: PropTypes.object,
    disabled: PropTypes.bool
  }

  // Defaults for props
  static defaultProps = {
    text: "",
    onPress: () => null,
    width: '80%',
    initialGradientColor: colors.blue,
    finalGradientColor: colors.lightenBlue,
    disabled: false
  }

  render () {
    return (
      <TouchableOpacity 
        disabled={this.props.disabled}
        activeOpacity={0.8}
        style={{width: this.props.width, marginTop: 20}} 
        onPress={this.props.onPress}>
        <LinearGradient 
          colors={this.props.disabled 
            ? [ colors.gray, colors.darkenGray ]
            : [ this.props.initialGradientColor, this.props.finalGradientColor ]} 
          style={styles.gradientButton}
          start={{x: 0, y: 0}} end={{x: 1, y: 0}}>
          <Text style={[styles.buttonText, this.props.textStyle]}>
            {this.props.text}
          </Text>
        </LinearGradient>
      </TouchableOpacity>
    )
  }
}
