import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native'
import styles from './styles/CardStyles'

export default class Card extends Component {

  // PropTypes
  static propTypes = {
    borderRadius: PropTypes.number,
    marginVertical: PropTypes.number,
    marginHorizontal: PropTypes.number,
    paddingVertical: PropTypes.number,
    paddingHorizontal: PropTypes.number,
    elevation: PropTypes.number,
  }

  static defaultProps = {
    borderRadius: 4,
    paddingHorizontal: 0,
    paddingVertical: 0,
    marginHorizontal: 0,
    marginVertical: 0,
    elevation: 0
  }

  render () {
    return (
      <View style={[styles.card, {
        marginHorizontal: this.props.marginHorizontal,
        marginVertical: this.props.marginVertical,
        paddingHorizontal: this.props.paddingHorizontal,
        paddingVertical: this.props.paddingVertical,
        borderRadius: this.props.borderRadius,
        elevation: this.props.elevation
      }]}>
        {this.props.children}
      </View>
    )
  }
}